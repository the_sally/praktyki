operator = int(
    input("Podaj jakiś kształt (1-trójkąt, 2-kwadrat, 3-prostokąt, 4-koło): "))


def triangle_circuit():
    a = int(input("Podaj a: "))
    b = int(input("Podaj b: "))
    c = int(input("Podaj c: "))
    if a+b > c and a+c > b and b+c > a:
        triangle_perimeter = a+b+c
        print(f"Obwód wynosi: {triangle_perimeter}")
    else:
        print("Nie da się zbudować takiego trójkąta")


def triangle_field():
    a = int(input("Podaj a: "))
    b = int(input("Podaj b: "))
    c = int(input("Podaj c: "))
    h = int(input("Podaj h: "))
    if a+b > c and a+c > b and b+c > a:
        triangle_area = (a*h)/2
        print(f"Pole wynosi: {triangle_area}")
    else:
        print("Nie da się zbudować takiego trójkąta")


def square_circuit():
    a = int(input("Podaj a: "))
    square_circuit = 4*a
    return square_circuit


def square_field():
    a = int(input("Podaj a: "))
    square_field = a**2
    return square_field


def rectangle_circuit(a, b):
    a = int(input("Podaj a: "))
    b = int(input("Podaj b: "))
    rectangle_circuit = (2*a) + (2*b)
    return rectangle_circuit


def rectangle_field():
    a = int(input("Podaj a: "))
    b = int(input("Podaj b: "))
    rectangle_field = a*b
    return rectangle_field


def circle_circuit():
    r = int(input("Podaj r: "))
    circle_circuit = 2*3.14*r
    return circle_circuit


def circle_field():
    r = int(input("Podaj r: "))
    circle_field = 3.14*(r**2)
    return circle_field


# a = int(input("Podaj a: "))
# b = int(input("Podaj b: "))
# c = int(input("Podaj c: "))
# h = int(input("Podaj h: "))
# r = int(input("Podaj r: "))
circuit = {
    1: triangle_circuit,
    2: square_field,
    3: rectangle_field,
    4: circle_field,
}

field = {
    1: triangle_field,
    2: square_field,
    3: rectangle_field,
    4: circle_field,
}

circuit = circuit.get(operator)
field = field.get(operator)

print(f"Obwód wynosi: {circuit()}")
print(f"Pole wynosi: {field()}")
