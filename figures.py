class Triangle:
    def __init__(self, a, b, c, h) -> None:
        self.a = a
        self.b = b
        self.c = c
        self.h = h

    def area(self):
        return (self.a * self.h) / 2

    def perimeter(self):
        return self.a + self.b + self.c

    def can_build_triangle(self):
        return (
            self.a + self.b > self.c
            and self.a + self.c > self.b
            and self.b + self.c > self.a
        )


class Rectangle:
    def __init__(self, a, b) -> None:
        self.a = a
        self.b = b

    def area(self):
        return self.a * self.b

    def perimeter(self):
        return (2 * self.a) + (2 * self.b)


class Square(Rectangle):
    def __init__(self, a) -> None:
        self.a = a
        self.b = a


class Circle:
    PI = 3.14

    def __init__(self, r) -> None:
        self.r = r

    def area(self):
        return self.PI * (self.r**2)

    def perimeter(self):
        return 2 * self.PI * self.r
