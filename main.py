from project3 import Circle, Square, Rectangle, Triangle
import typer

app = typer.Typer()


@app.command()
def calculate_triangle(
    a: int = typer.Argument(..., min=0),
    b: int = typer.Argument(..., min=0),
    c: int = typer.Argument(..., min=0),
    h: int = typer.Argument(..., min=0),
    area: bool = typer.Option(False),
    perimeter: bool = typer.Option(False),
):

    triangle = Triangle(a, b, c, h)
    if not triangle.can_build_triangle():
        print("Nie da się zbudować takiego trójkąta")
        return
    if area:
        triangle_area = triangle.area()
        print(f"Pole wynosi: {triangle_area}")
    elif perimeter:
        triangle_perimeter = triangle.perimeter()
        print(f"Obwód wynosi: {triangle_perimeter}")
    else:
        print("Nie wybrałeś żadnej opcji")


@app.command()
def calculate_square(
    a: int = typer.Argument(..., min=0),
    area: bool = typer.Option(False),
    perimeter: bool = typer.Option(False),
):
    square = Square(a)
    if area:
        square_area = square.area()
        print(f"Pole wynosi: {square_area}")
    elif perimeter:
        square_perimeter = square.perimeter()
        print(f"Obwód wynosi: {square_perimeter}")
    else:
        print("Nie wybrałeś żadnej opcji")


@app.command()
def calculate_rectangle(
    a: int = typer.Argument(..., min=0),
    b: int = typer.Argument(..., min=0),
    area: bool = typer.Option(False),
    perimeter: bool = typer.Option(False),
):
    rectangle = Rectangle(a, b)
    if area:
        rectangle_area = rectangle.area()
        print(f"Pole wynosi: {rectangle_area}")
    elif perimeter:
        rectangle_perimeter = rectangle.perimeter()
        print(f"Obwód wynosi: {rectangle_perimeter}")
    else:
        print("Nie wybrałeś żadnej opcji")


@app.command()
def calculate_circle(
    r: int = typer.Argument(..., min=0),
    area: bool = typer.Option(False),
    perimeter: bool = typer.Option(False),
):
    circle = Circle(r)
    if area:
        circle_area = circle.area()
        print(f"Pole wynosi: {circle_area}")
    elif perimeter:
        circle_perimeter = circle.perimeter()
        print(f"Obwód wynosi: {circle_perimeter}")
    else:
        print("Nie wybrałeś żadnej opcji")


# hello
if __name__ == "__main__":
    app()
